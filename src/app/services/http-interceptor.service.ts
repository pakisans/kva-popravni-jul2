import { Injectable } from '@angular/core';
import { LoginService } from "./login.service";
import { HttpInterceptor } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor(private loginService: LoginService) { }
  
  intercept(
    req: import("@angular/common/http").HttpRequest<any>,
    next: import("@angular/common/http").HttpHandler
  ): import("rxjs").Observable<import("@angular/common/http").HttpEvent<any>> {
    let newReq = req.clone({
      headers: req.headers.set("Authorization", this.loginService.getToken()),
    });
    return next.handle(newReq);
  }
}
