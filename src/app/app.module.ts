import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { UsersComponent } from './components/users/users.component';
import { UsersDetailsComponent } from './components/users-details/users-details.component';
import { UsersUpdateComponent } from './components/users-update/users-update.component';
import { RegisterComponent } from './components/register/register.component';
import { FilesComponent } from './components/files/files.component';
import { FilesAddComponent } from './components/files-add/files-add.component';
import { FilesUpdateComponent } from './components/files-update/files-update.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpInterceptorService } from './services/http-interceptor.service';
import { HomeComponent } from './components/home/home.component';
import { FilesDragDropComponent } from './components/files-drag-drop/files-drag-drop.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UsersComponent,
    UsersDetailsComponent,
    UsersUpdateComponent,
    RegisterComponent,
    FilesComponent,
    FilesAddComponent,
    FilesUpdateComponent,
    HomeComponent,
    FilesDragDropComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    DragDropModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
