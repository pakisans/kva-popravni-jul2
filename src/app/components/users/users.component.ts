import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from '@angular/core';
import { User } from "src/app/entitity";
import { LoginService } from "src/app/services/login.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(public loginService: LoginService, private http: HttpClient) { }

  users:User[];
  showForm:boolean = false;


  ngOnInit(): void {
    this.getUsers();
  }

  selectedUser: User;
  pass: string;

  getUsers(){
    this.http
      .get<User[]>(`http://localhost:8080/api/users`)
      .subscribe((res) => (this.users = res));
  }

  delete(id: number) {
    this.http
      .delete(`http://localhost:8080/api/users/${id}`)
      .subscribe((res) => {
        this.getUsers();
      });
  }

  update(user: User, id: number) {
    this.showForm = !this.showForm;
    this.selectedUser = user;
    this.selectedUser.id = id;
  }

  updateUser(user: User) {
    this.http
      .put(`http://localhost:8080/api/users/${user.id}`, user)
      .subscribe((res) => {
        this.showForm = false;
        this.getUsers();
      });
  }

  details(password: string) {
    this.pass = password;
  }



  

}
