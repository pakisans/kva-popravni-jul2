import { Component, OnInit } from '@angular/core';
import { File } from "src/app/entitity";
import { LoginService } from "src/app/services/login.service";
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit {

  constructor(private http: HttpClient, public loginService: LoginService) { }

  ngOnInit(): void {
    this.getFiles();
  }

  files: File[];

  showForm: boolean = false;

  d1Files: File[] = [];
  d2Files: File[] = [];

  manage_files() {
    for (let file of this.files) {
      if (file.directory == "d1") {
        this.d1Files.push(file);
      } else {
        this.d2Files.push(file);
      }
    }
  }

  getFiles() {
    this.http
      .get<File[]>(`http://localhost:8080/api/files`)
      .subscribe((res) => {
        this.files = res;
        this.manage_files();
      });
  }
  selectedFile: File;

  delete(id: number) {
    this.http
      .delete<File>(`http://localhost:8080/api/files/${id}`)
      .subscribe((_) => {
        this.getFiles();
      });
  }
  update(file: File, id: number) {
    this.showForm = !this.showForm;
    this.selectedFile = file;
  }

  updateFile(file: File) {
    console.log(file);
    this.http
      .put<File>(`http://localhost:8080/api/files/${file.id}`, file)
      .subscribe((res) => {
        this.getFiles();
        this.showForm = false;
      });
  }

  addNewFile(file: File) {
    this.http.post(`http://localhost:8080/api/files`, file).subscribe((res) => {
      this.getFiles();
    });
  }adsads

}
