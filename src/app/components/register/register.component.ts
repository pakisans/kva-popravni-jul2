import { Component, OnInit } from '@angular/core';
import { User } from "src/app/entitity";
import { RegisterService } from "src/app/services/register.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private registerService: RegisterService, private router: Router) { }

  ngOnInit(): void {
  }

  user: User = {
    id: null,
    username: "",
    password: "",
    roles: ["ROLE_USER"],
  };

  register() {
    this.registerService.register(this.user).subscribe((res) => {
      console.log(res);
      this.router.navigate(["users"]);
    });
  }

}
