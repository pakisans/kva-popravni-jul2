import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilesDragDropComponent } from './files-drag-drop.component';

describe('FilesDragDropComponent', () => {
  let component: FilesDragDropComponent;
  let fixture: ComponentFixture<FilesDragDropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilesDragDropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilesDragDropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
