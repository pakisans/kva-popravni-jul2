import { moveItemInArray, transferArrayItem } from "@angular/cdk/drag-drop";
import { Component, OnInit, Input } from '@angular/core';
import { File } from "../../entitity";
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-files-drag-drop',
  templateUrl: './files-drag-drop.component.html',
  styleUrls: ['./files-drag-drop.component.css']
})
export class FilesDragDropComponent implements OnInit {

  constructor(private http: HttpClient) { }

  @Input()
  d1: File[];

  @Input()
  d2: File[] = [];

  ngOnInit(): void {
  }

  getFiles() {
    this.http
      .get<File[]>(`http://localhost:8080/api/folders`)
      .subscribe((res) => (this.d1 = res));
  }

  drop(event) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }

    if (event.currentIndex == -1) {
      console.log(event.containerData);
    }
  }
  

}
