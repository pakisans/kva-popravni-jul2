import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { File } from "src/app/entitity";

@Component({
  selector: 'app-files-update',
  templateUrl: './files-update.component.html',
  styleUrls: ['./files-update.component.css']
})
export class FilesUpdateComponent implements OnInit {

  constructor() { }

  @Output() onUpdate: EventEmitter<File> = new EventEmitter();
  @Input()
  fileToUpdate: File;

  ngOnInit(): void {
  }

  sendEmit(){
    this.onUpdate.emit(this.fileToUpdate);
  }

}
