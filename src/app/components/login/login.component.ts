import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { User } from "src/app/entitity";
import { LoginService } from "src/app/services/login.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private loginService:LoginService,private router:Router) { }

  ngOnInit(): void {
  }

  user: User = {
  	id: null,
  	username: "",
  	password: "",
  };

  login(){
  	this.loginService.login(this.user).subscribe((res) => {
  		this.router.navigate(["/users"]);
  	});
  }
}
