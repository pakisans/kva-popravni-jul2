import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from "src/app/entitity"
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-users-update',
  templateUrl: './users-update.component.html',
  styleUrls: ['./users-update.component.css']
})
export class UsersUpdateComponent implements OnInit {

  constructor(private http: HttpClient) { }

  @Output() onSumbit: EventEmitter<User> = new EventEmitter();

  ngOnInit(): void {
  }

  @Input()
  updateUser: User;


  update(){
    this.onSumbit.emit(this.updateUser);
  }

}
