import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { File } from "src/app/entitity";

@Component({
  selector: 'app-files-add',
  templateUrl: './files-add.component.html',
  styleUrls: ['./files-add.component.css']
})
export class FilesAddComponent implements OnInit {
  
  @Output() onAdd: EventEmitter<File> = new EventEmitter();

  newFile: File = {
    id: null,
    title: "",
    directory: "",
    type: "",
    size: null,
  };


  constructor() { }

  ngOnInit(): void {
  }

  add(){
    this.onAdd.emit(this.newFile);
  }

  

}
