import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './components/users/users.component';
import { AuthGuard } from "./guards/auth.guard";
import { FilesComponent } from './components/files/files.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { FilesAddComponent } from './components/files-add/files-add.component';
import { FilesUpdateComponent } from './components/files-update/files-update.component';
import { UsersDetailsComponent } from './components/users-details/users-details.component';
import { UsersUpdateComponent } from './components/users-update/users-update.component';

const routes: Routes = [
  {
    path:"",
    component: HomeComponent,
  },
  {
    path:"users",
    component: UsersComponent,
    canActivate: [AuthGuard],
    data: {
      allowedRoles: ["ROLE_ADMIN", "ROLE_USER"],
    },
  },
  {
    path: "files",
    component: FilesComponent,
  },
  {
    path: "files-add",
    component: FilesAddComponent,
    canActivate: [AuthGuard],
    data: {
      allowedRoles: ["ROLE_USER"],
    },
  },
  {
    path: "files-update",
    component: FilesUpdateComponent,
    canActivate: [AuthGuard],
    data: {
      allowedRoles: ["ROLE_USER"],
    },
  },
  {
    path: "users-deatils",
    component: UsersDetailsComponent,
    canActivate: [AuthGuard],
    data: {
      allowedRoles: ["ROLE_ADMIN"],
    },
  },
  {
    path: "users-update",
    component: UsersUpdateComponent,
    canActivate: [AuthGuard],
    data: {
      allowedRoles: ["ROLE_ADMIN"],
    },
  },
  {
    path: "users",
    component: UsersComponent,
    canActivate: [AuthGuard],
    data: {
      allowedRoles: ["ROLE_ADMIN"],
    },
  },

  {
    path: "login",
    component: LoginComponent,
  },
  {
    path: "register",
    canActivate: [AuthGuard],
    component: RegisterComponent,
    data: {
      allowedRoles: ["ROLE_ADMIN"],
    },
  },
  {
    path: "**",
    component: HomeComponent,
  },
];
  

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
